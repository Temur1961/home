import axios from 'axios';

const ApiServices = axios.create({
   baseURL: 'https://cabinet.mdokon.uz/', 
  headers: {
    'Content-Type': 'application/json',
  }
 
}); 


ApiServices.interceptors.request.use(function (config) {
  let token = localStorage.getItem("token");
  if (token) {
    config.headers["Authorization"] = ["Bearer", token].join(" ");
  }
  return config;
}, function (error) {
  if (error && error.response && error.response.data && error.response.data.error) {
    return Promise.reject(error.response.data.error);
  } else {
    return Promise.reject(error);
  }
});

export default ApiServices;
