import ApiServices from '../api.services';

const SigninService = {
  signin(data) {
    return ApiServices.post('/auth/login', data);
  },
}

export default SigninService