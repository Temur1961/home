import React, { useState, useEffect } from "react";
import {
  Table,
  Tooltip,
  Space,
  Button,
  Form,
} from "antd";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Fade from "react-reveal/Fade";
import axios from "axios";
import Card from "../../../components/MainCard";
import classes from "./Page.module.css";

const Page = (props) => {
  const { t } = useTranslation();

  const [divisionData, setDivisionData] = useState([]);
  const [loader, setLoader] = useState(true);
  //  const [token, setToken] = useState([])

  const token ="eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJ0ZXN0bWQiLCJzY29wZSI6WyJvcGVuaWQiXSwibGFzdF9uYW1lIjoiUmFzdWxvdiIsImV4cCI6MTY2OTM1NjE0OSwiaWF0IjoxNjY5MzUyNTQ5LCJmaXJzdF9uYW1lIjoiSWJyb2hpbSIsImF1dGhvcml0aWVzIjpbIlJPTEVfT1dORVIiXSwianRpIjoiem9wRUxXLVJFWVY0MlBlS2JmNGNJbU84d0lBIiwiY2xpZW50X2lkIjoid2ViX2FwcCIsIm93bmVyTG9naW4iOm51bGwsImFjdGl2YXRlZCI6dHJ1ZX0.IyRXfImonq2IjdXIA6AmtbQqloXYhp5z9-lmKJh6IKMh0jkODGJyaWZcsLPEcA289-QSw5uLGIs7CF47Bs-e7WtdyYbTDajqng002pFqEsepcQ2LsSGyPSRfrBXlkotM0zwhYzVOfTWva-NeXadEi3N25yGAovFywLP3zbPURG0QvORM9lJkoTSWAkRcTSaPNV00e1KvQ9LPkfQHfGbGoJKhslAMq3U_jY1KRfV6FEejp_0u24u9lTyOQFQ0V5Mc9EZLSnpepl6S63tMJZ22SJSPTo2_xMxtpFQmcrsMcFAb9iW5dZyUevWde0YMiXHpi-SdL5hUvDqGwuZ1cXHgyg";
  useEffect(() => {
    const config = {
      headers: {
        Authorization: "Bearer " + token,
      },
    };
    axios
      .get(
        `https://cabinet.mdokon.uz/services/web/api/documents-in-pageList`,
        config
      )
      .then((result) => {
        setDivisionData(result.data);
        setLoader(false);
      });
    axios
      .get(`https://cabinet.mdokon.uz/services/web/api/pos-helper`, config)
      .then((result) => {
        setDivisionData(result.data);
        setLoader(false);
      });
  }, []);

  const columns = [
    {
      title: t("posName"),
      dataIndex: "posName",
      key: "posName",
      width: 200,
    },
    {
      title: t("organizationName"),
      dataIndex: "organizationName",
      key: "organizationName",
      width: 200,
    },
    {
      title: t("inNumber"),
      dataIndex: "inNumber",
      key: "inNumber",
      width: 200,
    },
    {
      title: t("totalAmount"),
      dataIndex: "totalAmount",
      key: "totalAmount",
      width: 200,
    },
    {
      title: t("createdDate"),
      dataIndex: "createdDate",
      key: "createdDate",
      width: 200,
    },
    {
      title: t("actions"),
      key: "action",
      width: 90,
      align: "center",
      fixed: "right",
      render: (record) => {
        return (
          <Space size="middle">
            <Tooltip title={t("Edit")}>
              <Link to={`${props.match.path}/${record.id}`}>
                <i
                  className="feather icon-edit action-icon"
                  aria-hidden="true"
                />
              </Link>
            </Tooltip>
          </Space>
        );
      },
    },
  ];
  return (
    <Card title={t("Page")}>
      <Fade>
        <Form className={classes.FilterWrapper}>
          <div className="main-table-filter-wrapper">
            <Form.Item>
              <Link
                // to={`${props.match.path}/add`}
                to={{
                  pathname: `${props.match.path}/add`,
                  token: token,
                }}
              >
                <Button type="primary">
                  {t("add-new")}&nbsp;
                  <i className="fa fa-plus" aria-hidden="true" />
                </Button>
              </Link>
            </Form.Item>
          </div>
        </Form>
      </Fade>
      <Fade>
        <Table
          columns={columns}
          bordered
          dataSource={divisionData}
          loading={loader}
          showSorterTooltip={false}
          rowKey={(record) => record.ID}
          rowClassName="table-row"
          scroll={{
            x: "max-content",
          }}
        />
      </Fade>
    </Card>
  );
};

export default Page;
