import React from "react";
import {Table} from "antd";
// import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

const Products = (dataToTable) => {
    console.log(dataToTable);
  const { t } = useTranslation();

  const columns = [
    {
      title: t("name"),
      dataIndex: "name",
      key: "name",
    },
    {
      title: t("barcode"),
      dataIndex: "barcode",
      key: "barcode",
    },
    // {
    //   title: t("actions"),
    //   key: "action",
    //   width: 90,
    //   align: "center",
    //   fixed: "right",
    //   render: (record) => {
    //     return (
    //       <Space size="middle">
    //         <Tooltip title={t("Edit")}>
    //           <Link to={`${props.match.path}/${record.id}`}>
    //             <i
    //               className="feather icon-edit action-icon"
    //               aria-hidden="true"
    //             />
    //           </Link>
    //         </Tooltip>
    //       </Space>
    //     );
    //   },
    // },
  ];
  return (
        <Table
          columns={columns}
          bordered
          dataSource={dataToTable}
        //   loading={loader}
          showSorterTooltip={false}
          rowKey={(record) => record.ID}
          rowClassName="table-row"
          scroll={{
            x: "max-content",
          }}
        />
  );
};

export default Products;
