import React, { useState, useEffect } from "react";
import { Row, Col, Form, Button, Input, Spin, } from "antd";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import Fade from 'react-reveal/Fade';

import Card from "../../../components/MainCard";
import classes from "./Page.module.css";
import { Notification } from "../../../../helpers/notifications";
import DepartmentServices from "../../../../services/References/Page/page.services";

const layout = {
  labelCol: {
    span: 24,
  },
  wrapperCol: {
    span: 24,
  },
};


//main function
const EditPage = (props) => {
  const { t } = useTranslation();
  const history = useHistory();
  const [form] = Form.useForm();
  const [divisionData, setDivisionData] = useState([]);
  const [loader, setLoader] = useState(true);
  //onfinish
  const onFinish = (values) => {
    DepartmentServices.putData(props.match.params.id, values)
      .then((res) => {
        if (res.status === 200) {
          history.push("/page");
          Notification("success", t("saved"));
        }
      })
      .catch((err) => {
        Notification('error', err);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log(errorInfo);
  };
  //useeffect


  useEffect(() => {
    async function fetchData() {
      try {
        const division = await DepartmentServices.getById(
          props.match.params.id
        );
        setDivisionData(division.data.payload); 
        setLoader(false);
      } catch (err) {
        alert(err);
        console.log(err);
      }
    }
    fetchData();

  }, [ props.match.params.id]);
  //useffect end

  //loader
  if (loader) {
    return (
      <div className="spin-wrapper">
        <Spin size='large'/>
      </div>
    );
  }
  //loader end

  //main
  return (
    <Fade bottom>
    <Card title={t("Edit Page")}>
      <Form
        {...layout}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        className={classes.FilterForm}
        form={form}
        initialValues={divisionData}
      >
        <Row gutter={[16, 16]}>        
          <Col xl={8} lg={12}>
            <Form.Item
              label={t("name")}
              name="name"
              rules={[
                {
                  required: true,
                  message: t('Please input valid'),
                },
              ]}
            >
              <Input placeholder={t("name")} />
            </Form.Item>
            <div className={classes.SwitchWrapper}>            
               <div className={classes.Buttons}>
                <Button
                  type="danger"
                  onClick={() => {
                    history.goBack();
                    Notification("success", t("not-saved"));
                  }}
                >
                  {t("back")}
                </Button>
                <Button type="primary" htmlType="submit">
                  {t("save")}
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </Form>
    </Card>
    </Fade>
  );
};

export default EditPage;
