import React from "react";
import { Row, Col, Form, Button, Select, Input, Space, Table, AutoComplete } from "antd";
// import { CSSTransition } from 'react-transition-group';
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import Fade from "react-reveal/Fade";
import axios from "axios";
import Card from "../../../components/MainCard";
import classes from "./Page.module.css";
import { Notification } from "../../../../helpers/notifications";
import { useEffect } from "react";
import { useState } from "react";
import { QrcodeOutlined, PlusCircleOutlined } from "@ant-design/icons";
// import FormItem from "antd/lib/form/FormItem";
// import reduxSaga from "redux-saga";
// import TableModal from "./Modal"
// import ProductsTable from "./ProductsTable"

const layout = {
  labelCol: {
    span: 24,
  },
  wrapperCol: {
    span: 24,
  },
};
const { Option } = Select;

const AddPage = (props) => {
  const { t } = useTranslation();
  const history = useHistory();
  const [form] = Form.useForm();
  const [tableForm] = Form.useForm();
  const [data, setData] = useState([]);
  const [payment, setPayment] = useState([]);
  const [shop, setShop] = useState([]);
  const [tableData, setTableData] = useState([]);
  const [barCode, setBarCode] = useState(false);

  const [ModalVisible, setModalVisible] = useState(true);
  const [ModalData, setModalData] = useState([]);
  const [dataToTable, setDataToTable] = useState([]);

  useEffect(() => {
    const config = {
      headers: {
        Authorization: "Bearer " + props.location.token,
      },
    };

    axios
      .get(
        `https://cabinet.mdokon.uz/services/web/api/organization-helper`,
        config
      )
      .then((result) => {
        // console.log(result.data, "helper");
        setData(result.data);
        // setDivisionData(result.data)
        // setLoader(false)
      });
    axios
      .get(
        `https://cabinet.mdokon.uz/services/web/api/product-category-helper`,
        config
      )
      .then((result) => {
        setPayment(result.data);
        // setDivisionData(result.data)
        // setLoader(false)
      });
    axios
      .get(`https://cabinet.mdokon.uz/services/web/api/pos-helper`, config)
      .then((result) => {
        setShop(result.data);
        // setDivisionData(result.data)
        // setLoader(false)
      });
    // axios
    //   .get(`https://cabinet.mdokon.uz/services/web/api/product-in-helper?name=cola&posId=141&barcode=false&currencyId=1`, config)
    //   .then((result) => {
    //     //setShop(result.data);
    //     setModalData(result?.data)
    //     console.log(result.data);
    //     // setLoader(false)
    //   });
  }, [setModalData]);


  const onFinish = (values) => {
    // console.log(values);
    // values.id = 0;

  };

  const column = [
    {
      title: t("name"),
      dataIndex: "name",
      key: "nameID",
    },
    {
      title: t("barcode"),
      dataIndex: "barcode",
      key: "barcode",
    },
    {
      title: t("balance"),
      dataIndex: "balance",
      key: "balance",
      width: "10%",
    },
    {
      title: t("Soni"),
      dataIndex: "Soni",
      key: "Soni",
      width: "5%",
    },
    {
      title: t("uomName"),
      dataIndex: "uomName",
      key: "uomName",
    },
    {
      title: t("price"),
      dataIndex: "price",
      key: "price",
      width: "10%",
    },
    {
      title: t("wholesalePrice"),
      dataIndex: "wholesalePrice",
      key: "wholesalePrice",
      editable: true,
      width: "10%",

    },
    {
      title: t("salePrice"),
      dataIndex: "salePrice",
      key: "salePrice",
      editable: true,
      width: "10%",

    },
    {
      title: t("productDB"),
      dataIndex: "productDB",
      key: "productDB",
      width: "5%",
    },
    {
      title: t("Muddati"),
      dataIndex: "Muddati",
      key: "Percentage",
      width: "5%",
      editable: true,
    },
    {
      title: t("QQS"),
      dataIndex: "Percentage",
      key: "Percentage",
      width: "5%",
    },
    {
      title: t("Amallar"),
      dataIndex: "Percentage",
      key: "Percentage",
      width: "5%",
      editable: true,
    },
  ];
  //table



  const onSearch = (e) => {
    console.log(e);
    // Send data to the backend via POST
    axios.get(`https://cabinet.mdokon.uz/services/web/api/product-in-helper?name=${e}&posId=141&barcode=${barCode}&currencyId=1`, {  // Enter your IP address here

      // method: 'GET',
      headers: {
        'Authorization': `Bearer ${props.location.token}`, // notice the Bearer before your token
      },

    }) 

      .then((result) => {
      if (e) {
        setModalData(result.data);
      }

    });

    
  }

  // let selectTable = null;

  // const searchItem = (e) => {
  //   console.log(e);
  //   onSearch();
  //   if (e) {
  //     selectTable = (
  //       <ProductsTable dataToTable={dataToTable}/>
  //     )
  //   }
  // };
  

const tableDataAddHabdler = (values) => {
  console.log(values);
  // const newTable = {...tableData}
  // setTableData1((tableData1) => [...tableData1, tableData1]);
  // console.log(newTable);
}

const barCodeHabdler = () => {
  setBarCode(true)
}

return (
  <Fade bottom>
    <Card title={t("Mahsulot Qoshish ")}>
      <Form
        {...layout}
        onFinish={onFinish}
        className={classes.FilterForm}
        form={form}
        initialValues={{
          ForStaffList: false,
        }}
      >
        <Row gutter={[16, 16]}>
          <Col xl={4} lg={12}>
            <Form.Item
              label={t("Savdo nuqtasi")}
              name="Savdo_id"

              rules={[
                {
                  required: true,
                  message: t("Please select"),
                },
              ]}
            >
              <Select
                disabled={tableData.length !== 0}
                placeholder={t("Select")}
                allowClear
                getPopupContainer={(trigger) => trigger.parentNode}
                showSearch
                //onSelect={elementHandler}
                filterOption={(input, option) =>
                  option.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
              >
                {shop.map((item) => {
                  return (
                    <Option key={item.id} value={item.id}>
                      {item.name}
                    </Option>
                  );
                })}
              </Select>
            </Form.Item>
          </Col>
          <Col xl={4} lg={12}>
            <Form.Item
              label={t("Ta'minotchi")}
              name="taminotchi_id"
              rules={[
                {
                  required: true,
                  message: t("Please select"),
                },
              ]}
            >
              <Select
                disabled={tableData.length !== 0}
                placeholder={t("Select")}
                allowClear
                showSearch
                filterOption={(input, option) =>
                  option.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
              >
                {data.map((res) => {
                  return <option key={res.id}>{res.name}</option>;
                })}


              </Select>
            </Form.Item>
          </Col>
          <Col xl={4} lg={12}>
            <Form.Item
              label={t("Valyuta")}
              name="valyuta_id"
              rules={[
                {
                  required: false,
                  message: t("Please select"),
                },
              ]}
            >
              <Select
                placeholder={t("Select")}
                allowClear
                getPopupContainer={(trigger) => trigger.parentNode}
                showSearch
                //onSelect={elementHandler}
                filterOption={(input, option) =>
                  option.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
              >
                <Option key='1'>UZS</Option>
                <Option key='2'>USD</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col xl={4} lg={12}>
            <Form.Item
              label={t("To'lov uslubi")}
              name="tolov_id"
              rules={[
                {
                  required: false,
                  message: t("Please select"),
                },
              ]}
            >
              <Select
                placeholder={t("Select")}
                allowClear
                getPopupContainer={(trigger) => trigger.parentNode}
                showSearch
                //onSelect={elementHandler}
                filterOption={(input, option) =>
                  option.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
              >
                <Option key='1'>Naqd</Option>
                <Option key='2'>Pul O'tkazma</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col xl={4} lg={12}>
            <Form.Item
              label={t("№ Qabul qilish")}
              name="qabul_id"
              rules={[
                {
                  required: true,
                  message: t("Please select"),
                },
              ]}
            >
              <Input placeholder={t("name")} />
            </Form.Item>
          </Col>
          <Col xl={4} lg={12}>
            <Form.Item
              label={t("Izoh")}
              name="izoh"
              rules={[
                {
                  required: true,
                  message: t("Please select"),
                },
              ]}
            >
              <Input placeholder={t("name")} />
            </Form.Item>
          </Col>
          <Col xl={4} lg={12}>
            <Form.Item
              label={t("Ustama ulgurji")}
              name="ulgurji_id"
              rules={[
                {
                  required: true,
                  message: t("Please select"),
                },
              ]}
            >
              <Input placeholder={t("name")}
                addonAfter={
                  <div style={{ width: 40 }}>
                    %
                  </div>
                }
              />
            </Form.Item>
          </Col>
          <Col xl={4} lg={12}>
            <Form.Item
              label={t("Ustama sotish")}
              name="sotish_id"
              rules={[
                {
                  required: true,
                  message: t("Please select"),
                },
              ]}
            >
              <Input placeholder={t("name")}
                addonAfter={
                  <div style={{ width: 40 }}>
                    %
                  </div>
                }
              />
            </Form.Item>
          </Col>
          <Col xl={4} lg={12}>
            <Form.Item
              label={t("QQS")}
              name="qqs_id"
              rules={[
                {
                  required: true,
                  message: t("Please select"),
                },
              ]}
            >
              <Input placeholder={t("name")} />
            </Form.Item>
          </Col>
          <Col xl={4} lg={12}>
            <Form.Item
              label={t("Kategoriya")}
              name="category_i"
              rules={[
                {
                  required: true,
                  message: t("Please select"),
                },
              ]}
            >
              <Select
                placeholder={t("Select Division")}
                allowClear
                getPopupContainer={(trigger) => trigger.parentNode}
                showSearch
                //onSelect={elementHandler}
                filterOption={(input, option) =>
                  option.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
              >
                {payment.map((item) => {
                  return (
                    <Option key={item.id} value={item.id}>
                      {item.name}
                    </Option>
                  );
                })}
              </Select>
            </Form.Item>
          </Col>

          {/* <FormItem> */}
          <Col xl={24} lg={24}>
            <Select
                showSearch
                allowClear
                showArrow={false}
                // filterOption={(input, option) =>
                //   option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                // }
                onSearch={onSearch}
                // onChange={handleChange}
                notFoundContent={null}
                defaultActiveFirstOption={false}
                style={{
                  width: "calc(100% - 200px)",
                  height: "40px",
                }}
                
              >

                {ModalData.map(item => {
                  return (
                    <Option key={item.ID} value={item.productId}>
                      {item.name}
                    <span>+</span>
                    </Option>
                    // <Table
                    // bordered
                    // size="middle"
                    // rowClassName="table-row"
                    // className="main-table"
                    // // columns={columns}
                    // dataSource={item}
                    // />
                  )
                })}

              </Select>
            {/* <AutoComplete
              style={{ width: '80%' }}
              onChange={onSearch}
              options={<ProductsTable/>}
            /> */}
            <Button
              style={{
                height: "32px",
              }}

            // onClick={() => {
            //   form.validateFields()
            //   .then(() => {
            //     const newData = {...}
            //   });
            // }}
            >
              <PlusCircleOutlined />
            </Button>
            <Button
              style={{
                height: "32px",
                color: 'blue',
              }}
              onClick={barCodeHabdler}
            >
              <QrcodeOutlined />
            </Button>

          </Col>

          <Col xl={24} lg={24}>
            <Form
              form={tableForm}
              component={false}
            >

              <Table
                columns={column}
                className="main-table inner-table"
                dataSource={tableData}
              />


            </Form>
          </Col>

          <Col xl={24} lg={12}>
            <div className={classes.SwitchWrapper}>
              <div className={classes.Buttons}>
                <Button
                  type="danger"
                  onClick={() => {
                    history.goBack();
                    Notification("warning", t("not-saved"));
                  }}
                >
                  {t("back")}
                </Button>
                <Button type="primary" htmlType="submit">
                  {t("save")}
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </Form>
    </Card>
  </Fade >
);
  // }
};

export default AddPage;
