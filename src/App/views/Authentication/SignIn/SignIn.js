import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import { Form, Input, Button } from "antd";
import Fade from 'react-reveal/Fade';
import { useTranslation } from "react-i18next";
// import i18next from "i18next";

import "../../../../assets/scss/style.scss";
import classes from "./SignIn.module.css";
import Breadcrumb from "../../../layout/AdminLayout/Breadcrumb";
import SigninService from "../../../../services/Signin/signin";
// import { Notification } from "../../../../helpers/notifications";
// import DEMO from "../../../../store/constant";
// import logo from "../../../../assets/images/smartbank.png";

const layout = {
  labelCol: { span: 24 },
  wrapperCol: { span: 24 },
};

const SignIn = (props) => {
  const [isSignedIn, setSignedIn] = useState(false);
  const [loading, setLoading] = useState(false);

  const { t } = useTranslation();
  const [singinForm] = Form.useForm();

  const onFinish = (values) => {
 
    SigninService.signin(values)

      .then((response) => {
        console.log(response);
          localStorage.setItem("token", "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJtZG9rb250ZXN0Iiwic2NvcGUiOlsib3BlbmlkIl0sImxhc3RfbmFtZSI6IiIsImV4cCI6MTY2ODQ5MzQzOSwiaWF0IjoxNjY4NDg5ODM5LCJmaXJzdF9uYW1lIjoibURva29uIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9PV05FUiJdLCJqdGkiOiJJVkEzenhfWWdOSFM4cU1XbERvdkNrbjJXTDAiLCJjbGllbnRfaWQiOiJ3ZWJfYXBwIiwib3duZXJMb2dpbiI6bnVsbCwiYWN0aXZhdGVkIjp0cnVlfQ.IWV8y2a9e4H_hGBo2b1zQEtJ2LMxFSuDKu_Xl1UkFrsp_TLX51Ss1_qmBs_42ycLzD0PlPb7VeERnCTLpNBYvOMg4q8CwxZvm7hvKbLMENUIg86LPWfL4Ziel2EpOuz2pWgMs3EkIXTWI2TC6cJs26A7Fn0CqmwlKiBLpWYl3sRgU9GPKjxdcTAbjAeEnc-sZDUPn_SObIxgU61VqmoDb36o6i9v0Ge2uk714N0BWunjfl0ZuVgD7ulpPW_V0yvmvdQTxj9ABDjdpYU-KOl0DicJW_wG2KM_JQjJ46UXKZDbR2ARWxZBhjQEhOns2h4n6pb_IRNPolxkCghhUK0CfQ");
          //localStorage.setItem("userInfo", JSON.stringify(response.data.userinfo));
          setSignedIn(true);
      
      })
      .catch((error) => {
        console.log(error);
        Notification('error', error);
        setLoading(false);
      });
  };








  return (
    <>
      {isSignedIn && <Redirect to="/" exact />}
      <Breadcrumb />
      <Fade>
        <div className="auth-wrapper">
          <div className="auth-content">
            <div className="auth-bg">
              <span className="r" />
              <span className="r s" />
              <span className="r s" />
              <span className="r" />
            </div>
            <div className="card">
              <div className="card-body">
                <div className="text-center">
                  <div className="mb-4">
                    <i className="feather icon-unlock auth-icon" />
                  </div>
                  <h3 className="mb-4">{t("login")}</h3>
               
                </div>
                <Form
                  {...layout}
                  form={singinForm}
                  name="basic"
                  initialValues={{ remember: true }}
                  onFinish={onFinish}
                //   onFinishFailed={onFinishFailed}
                >
                  <Form.Item
                    label={t("username")}
                    name="username"
                    rules={[
                      { required: true, message: t("usernameVerification") },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label={t("password")}
                    name="password"
                    rules={[
                      { required: true, message: t("passwordVerification") },
                    ]}
                  >
                    <Input.Password />
                  </Form.Item>

                  <Form.Item className={classes.Button}>
                    <Button type="primary" htmlType="submit" loading={loading}>
                      {t("signIn")}
                    </Button>
                  </Form.Item>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </Fade>

    </>
  );
};

export default SignIn;
