import React from 'react';

//Nav
//const ChangePwd = React.lazy(() => import('../App/views/Users/ChangePwd'));
//Nav end

//dashboard const
const Dashboard = React.lazy(() => import('../App/views/MainPage/index'));
//dashboard const end

//References const

const page = React.lazy(() => import('../App/views/References/Page/Page'));
const addpage = React.lazy(() => import('../App/views/References/Page/AddPage'));
const editpage = React.lazy(() => import('../App/views/References/Page/EditPage'));


//const Faq = React.lazy(() => import('../App/views/Faq/Faq'));
//others const end

const routes = [
  //{ path: '/faq', exact: true, name: 'faq', component: Faq },
 
  { path: '/dashboard/default', exact: true, name: 'Default', component: Dashboard, },
  //dashbord end

  { path: '/documents-in', exact: true, name: 'page', component: page,  },
  { path: '/documents-in/add', exact: true, name: 'addpage', component: addpage, },
  { path: '/documents-in/:id', exact: true, name: 'editpage', component: editpage, },

]

export default routes;
