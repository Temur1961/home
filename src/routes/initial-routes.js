import React from 'react';

const AdminLayout = React.lazy(() => import('../App/layout/AdminLayout'));
//const AdminLayout = React.lazy(() => import('./layout/AdminLayout'));

const route = [
  { path: '/dashboard/default', exact: true, name: 'AdminLayout', component: AdminLayout },
];

export default route;