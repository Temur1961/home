import { combineReducers } from "redux";

//References
//Orgabizational reference
//import subjectLnBLGHTReducer from "../App/views/References/Organizational/SubjectInBLGHT/_redux/getListSlice";
//Orgabizational reference
//References end

import navigationReducer from "./navigation-slice";
//import userListReducer from "../App/views/Documents/Admin/User/_redux/usersSlice";
//import salaryCalcGetListReducer from "../App/views/Documents/EmployeeMovement/SalaryCalculation/_redux/getListSlice";
//import PlasticCardSheetForMilitaryReducer from "../App/views/Military/PlasticCardSheetForMilitary/_redux/getListSlice";
//import basicEduPlanGetListReducer from "../App/views/References/Organizational/BasicEducationalPlan/_redux/getListSlice";
//import organizationListReducer from "../App/views/Documents/Admin/Organization/_redux/organizationsSlice";
//import rolesListReducer from "../App/views/Documents/Admin/Roles/_redux/rolesSlice";
//import changeDocSatusReducer from "../App/views/Documents/Admin/ChangeDocStatus/_redux/changeDocStatusSlice";
//import employeeListReducer from "../App/views/References/Organizational/Employee/_redux/EmployeeSlice";
// import timeSheetListReducer from "../App/views/Documents/Personnel_accounting/TimeSheet/_redux/TimeSheetSlice";
// import timeSheetEduListReducer from "../App/views/Documents/Personnel_accounting/TimeSheetEdu/_redux/TimeSheetEduSlice";
//import personnelDepartmentListReducer from "../App/views/Report/PersonnelDepartment/_redux/personnelDepartmentSlice";
// Documents
//import payrollOfPlasticCardSheetReducer from "../App/views/Documents/EmployeeMovement/PayrollOfPlasticCardSheet/_redux/getListSlice";
// Documents end
//import prefOrgsListReducer from "../App/views/Documents/Admin/PreferentialOrganization/_redux/prefOrgsSlice";
// import appointQualCategoryReducer from "../App/views/References/Organizational/AppointQualCategory/_redux/getListSlice";
// //import UserErorReducer from "../App/views/Admin/UserError/_redux/getListSlice";
// import subalcKindReducer from "../App/views/References/Organizational/SubCalculationKind/_redux/getListSlice";
// import positionOwnerReducer from "../App/views/References/Organizational/PositionOwner/_redux/getListSlice";

// import calcKindReducer from "../App/views/References/Global/CalculationKind/_redux/getListSlice";
// import itemOfExpenseReducer from "../App/views/References/Global/ItemOfExpense/_redux/getListSlice";
// import allPositionsReducer from "../App/views/References/Global/AllPositions/_redux/getListSlice";
// import taxReliefReducer from "../App/views/References/Global/TaxRelief/_redux/getListSlice";
// Templates
// import TPSubcalcKindReducer from "../App/views/References/Template/TPSubCalculationKind/_redux/getListSlice";
// import TPListOfPosCategoryReducer from "../App/views/References/Template/TPListOfPositionCategory/_redux/getListSlice";
// import TPListOfPosReducer from "../App/views/References/Template/TPListOfPosition/_redux/getListSlice";
// import TPBasicSubcalcKindReducer from "../App/views/References/Template/TPBasicSubCalculationKind/_redux/getListSlice";
// import TPWorkScheduleReducer from "../App/views/References/Template/TPWorkSchedule/_redux/getListSlice";
// import TPSalaryTransactionReducer from "../App/views/References/Template/TPSalaryTransaction/_redux/getListSlice";
// import TPLimitBySubCalculationKindReducer from "../App/views/References/Template/TPLimitBySubCalculationKind/_redux/getListSlice";
// import TPTaxesAndChargesReducer from "../App/views/References/Template/TPTaxesAndCharges/_redux/getListSlice";
// Templates end
//Reports
//import memorialOrder5Reducer from "../App/views/Report/MemorialOrder5/_redux/getListSlice";
//Express info
//import employeeCardListReducer from "../App/views/ExpressInfo/EmployeeCard/_redux/getListSlice";

export const rootReducer = combineReducers({
  //References
  //Organizational References
  //subjectsInBLGHTList: subjectLnBLGHTReducer,
  //Organizational References end
  //References end
  navigation: navigationReducer,
  // userList: userListReducer,
  // salaryCalcGetList: salaryCalcGetListReducer,
  //plasticCardSheetForMilitaryGetList: PlasticCardSheetForMilitaryReducer,
  //basicEduPlanGetList: basicEduPlanGetListReducer,
  // organizationList: organizationListReducer,
  // rolesList: rolesListReducer,
  // changeDocSatus: changeDocSatusReducer,
  //employeeList: employeeListReducer,
  // timeSheetList: timeSheetListReducer,
  // timeSheetEduList: timeSheetEduListReducer,
  //personnelDepartmentList: personnelDepartmentListReducer,
  //prefOrgsList: prefOrgsListReducer,
  // appointQualCategoryList: appointQualCategoryReducer,
  // userErrorList: UserErorReducer,
  // subalcKindList: subalcKindReducer,
  // positionOwnerList: positionOwnerReducer,
  // Documents
  // payrollOfPlasticCardSheetList: payrollOfPlasticCardSheetReducer,
  // Documents end
  // Templates
  // tpWorkScheduleList: TPWorkScheduleReducer,
  // tpTaxesAndChargesList: TPTaxesAndChargesReducer,
  // tpSalaryTransactionList: TPSalaryTransactionReducer,
  // tpSubcalcKindList: TPSubcalcKindReducer,
  // tpBasicSubcalcKindList: TPBasicSubcalcKindReducer,
  // tpLimitBySubCalcKindList: TPLimitBySubCalculationKindReducer,
  // tpListOfPosList: TPListOfPosReducer,
  // tpListOfPosCategoryList: TPListOfPosCategoryReducer,
  // // Templates end
  // calcKindList: calcKindReducer,
  // itemOfExpenseList: itemOfExpenseReducer,
  // allPositionsList: allPositionsReducer,
  // taxReliefList: taxReliefReducer,
  //Reports
  //memorialOrder5List: memorialOrder5Reducer,
  //Express info
  //employeeCardList: employeeCardListReducer,
});