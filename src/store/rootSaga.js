import { all, call } from "redux-saga/effects";

//References
//Organizational 
//import { subjectsInBLGHTSagas } from "../App/views/References/Organizational/SubjectInBLGHT/_redux/SubjectInBLGHTSaga";
//Organizational end
//References end
//all sagas
// import { userListSagas } from "../App/views/Documents/Admin/User/_redux/userListSaga";
// import { salaryCalcSagas } from "../App/views/Documents/EmployeeMovement/SalaryCalculation/_redux/salaryCalcSaga";
//import { PlasticCardSheetForMilitarySagas } from "../App/views/Military/PlasticCardSheetForMilitary/_redux/PlasticCardSheetForMilitarySaga";
// import { basicEduPlanSagas } from "../App/views/References/Organizational/BasicEducationalPlan/_redux/basicEduPlanSaga";
// import { organizationListSagas } from "../App/views/Documents/Admin/Organization/_redux/organizationListSaga";
// import { rolesListSagas } from "../App/views/Documents/Admin/Roles/_redux/rolesListSaga";
// import { changeDocStatusSagas } from "../App/views/Documents/Admin/ChangeDocStatus/_redux/changeDocStatusSaga";
// import { employeeListSagas } from "../App/views/References/Organizational/Employee/_redux/EmployeeSaga";
// import { timeSheetListSagas } from "../App/views/Documents/Personnel_accounting/TimeSheet/_redux/TimeSheetSaga";
// import { timeSheetEduListSagas } from "../App/views/Documents/Personnel_accounting/TimeSheetEdu/_redux/TimeSheetEduSaga";
//import { personnelDepartmentListSagas } from "../App/views/Report/PersonnelDepartment/_redux/personnelDepartmentSaga";
//import { prefOrgsListSagas } from "../App/views/Documents/Admin/PreferentialOrganization/_redux/prefOrgsListSaga";
// import { appointQualCategorySagas } from "../App/views/References/Organizational/AppointQualCategory/_redux/getListSaga";
//import { UserErrorSagas } from "../App/views/Admin/UserError/_redux/UserErrorSaga";
// import { subCalcKindSagas } from "../App/views/References/Organizational/SubCalculationKind/_redux/getListSaga";
// import { positionOwnerSagas } from "../App/views/References/Organizational/PositionOwner/_redux/getListSaga";

// import { calcKindSagas } from "../App/views/References/Global/CalculationKind/_redux/getListSaga";
// import { itemOfExpenseSagas } from "../App/views/References/Global/ItemOfExpense/_redux/getListSaga";
// import { allPositionsSagas } from "../App/views/References/Global/AllPositions/_redux/getListSaga";
// import { taxReliefSagas } from "../App/views/References/Global/TaxRelief/_redux/getListSaga";

// Documents
//import { payrollOfPlasticCardSheetSagas } from '../App/views/Documents/EmployeeMovement/PayrollOfPlasticCardSheet/_redux/payrollOfPlasticCardSheetSagas'
// Documents end
// Templates
// import { TPSubCalcKindSagas } from "../App/views/References/Template/TPSubCalculationKind/_redux/getListSaga";
// import { TPListOfPosCategorySagas } from "../App/views/References/Template/TPListOfPositionCategory/_redux/getListSaga";
// import { TPListOfPosSagas } from "../App/views/References/Template/TPListOfPosition/_redux/getListSaga";
// import { TPBasicSubCalcKindSagas } from "../App/views/References/Template/TPBasicSubCalculationKind/_redux/getListSaga";
// import { TPWorkScheduleSagas } from "../App/views/References/Template/TPWorkSchedule/_redux/getListSaga";
// import { TPSalaryTransactionSagas } from "../App/views/References/Template/TPSalaryTransaction/_redux/getListSaga";
// import { TPLimitBySubCalculationKindSagas } from "../App/views/References/Template/TPLimitBySubCalculationKind/_redux/getListSaga";
// import { TPTaxesAndChargesSagas } from "../App/views/References/Template/TPTaxesAndCharges/_redux/getListSaga";
// Templates end
//Reports
//import { memorialOrder5Sagas } from "../App/views/Report/MemorialOrder5/_redux/getListSaga";
//Express info
//import { employeeCardListSagas } from "../App/views/ExpressInfo/EmployeeCard/_redux/getListSaga";

export default function* rootSaga() {
  yield all([
    //References
    //Organizational
    // call(subjectsInBLGHTSagas),
    //Organizational end
    //References end
    //call(salaryCalcSagas),
    //call(PlasticCardSheetForMilitarySagas),
    // call(basicEduPlanSagas),
    // call(userListSagas),
    // call(organizationListSagas),
    // call(rolesListSagas),
    // call(changeDocStatusSagas),
    // call(employeeListSagas),
    // call(timeSheetListSagas),
    // call(timeSheetEduListSagas),
    //call(personnelDepartmentListSagas),
    //call(prefOrgsListSagas),
    // call(appointQualCategorySagas),
    // //call(UserErrorSagas),
    // call(subCalcKindSagas),
    // call(positionOwnerSagas),
    // // Documents
    // call(payrollOfPlasticCardSheetSagas),
    // Documents end
    // Templates
    // call(TPWorkScheduleSagas),
    // call(TPTaxesAndChargesSagas),
    // call(TPSalaryTransactionSagas),
    // call(TPSubCalcKindSagas),
    // call(TPBasicSubCalcKindSagas),
    // call(TPLimitBySubCalculationKindSagas),
    // call(TPListOfPosSagas),
    // call(TPListOfPosCategorySagas),
    // // Templates end
    // call(calcKindSagas),
    // call(itemOfExpenseSagas),
    // call(allPositionsSagas),
    // call(taxReliefSagas),
    //Reports
    //call(memorialOrder5Sagas),
    //Express info
   // call(employeeCardListSagas),
  ]);
}